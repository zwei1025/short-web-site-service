package com.zwx.service;

import com.zwx.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Title 短网址重定向服务
 */
@Service
public class RedirectService {
    @Autowired
    private RedisUtil redisUtil;

    public String redirectIndex(String key) {
        String url = redisUtil.get("shortUrl:" + key);
        url = url == null ? "/error/404" : url;
        return "redirect:" + url;
    }
}
