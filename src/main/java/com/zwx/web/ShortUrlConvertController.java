package com.zwx.web;

import com.zwx.response.ResultBean;
import com.zwx.service.ShortUrlConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Title 短网址转换服务
 */
@RestController
@RequestMapping("/shortUrl")
public class ShortUrlConvertController {
    @Autowired
    private ShortUrlConvertService shortUrlConvertService;

    /**
     * 短网址加密生成(为空默认有效期7天，可自定义时长)
     *
     * @param urlLong
     * @param days    为空默认7天
     * @return
     */
    @GetMapping("/shorten")
    public ResultBean shorten(String urlLong, Integer days) {
        return shortUrlConvertService.shorten(urlLong, days);
    }

    /**
     * 短网址解密
     *
     * @param url
     * @return
     */
    @GetMapping("/shortde")
    public ResultBean shortde(String url) {
        return shortUrlConvertService.shortde(url);
    }
}
